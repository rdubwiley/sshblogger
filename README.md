# SSHblogger: a Bubbletea Component for Markdown Blogs in the Terminal
![](./screenshots/blog.gif)

## Motivation
There's a list bubble component implemented [here](https://github.com/charmbracelet/bubbles/tree/master/list) and [glamour](https://github.com/charmbracelet/glamour) renders markdown in the terminal.

Let's put them together and make a blog!

## Quickstart for your terminal
```go
import (
    "fmt"
    "os"

    tea "github.com/charmbracelet/bubbletea"
    "github.com/charmbracelet/lipgloss"
    "gitlab.com/rdubwwily/sshblogger"
)

m := NewBlogModel(sshblogger.DemoBlogItems, "Ryan's Blog!", 100, 25, 10, lipgloss.NewStyle())
p := tea.NewProgram(m)
if _, err := p.Run(); err != nil {
    fmt.Printf("Alas, there's been an error: %v", err)
    os.Exit(1)
}
```

## How to use the component
#### Here's the new blog model function:
```go
func NewBlogModel(getBlogItems func() []BlogItem, title string, width int, height int, markdownHeight int, liststyle lipgloss.Style) BlogModel {
    m := BlogModel{
        GetBlogItems: getBlogItems,
        List: list.New(
            []list.Item{},
            list.NewDefaultDelegate(),
            width,
            height,
        ),
        Width:            width,
        Height:           height,
        ListRenderer:     liststyle,
        MarkdownHeight:   markdownHeight,
        contentLineStart: 0,
        contentLineEnd:   markdownHeight - 1,
    }
    m.List.Title = title
    return m
}
```
Biggest thing is to supply a function that gives a list of blog items, which is turned into list items.


Check out an implementation of serving the blog over wish with a sqlite DB [here](https://gitlab.com/rdubwiley/ssh-blog)