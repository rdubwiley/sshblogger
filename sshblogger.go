package sshblogger

import (
	"fmt"
	"strings"

	"github.com/charmbracelet/bubbles/list"
	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/glamour"
	"github.com/charmbracelet/lipgloss"
)

type item struct {
	title, desc string
}

func (i item) Title() string       { return i.title }
func (i item) Description() string { return i.desc }
func (i item) FilterValue() string { return i.title }

type RefreshBlogMsg struct{}

type BlogResizeMsg struct {
	width  int
	height int
}

type BlogItem struct {
	Id          int
	Title       string
	Description string
	Content     string
}

type BlogModel struct {
	List             list.Model
	BlogItems        []BlogItem
	BlogTitle        string
	GetBlogItems     func() []BlogItem
	IsViewing        bool
	CurrentContent   []string
	Width            int
	Height           int
	ListRenderer     lipgloss.Style
	glamourRenderer  *glamour.TermRenderer
	MarkdownHeight   int
	contentLineStart int
	contentLineEnd   int
}

func GenerateLongItem() string {
	startString := "### This is row 0\n"
	for i := 0; i < 49; i++ {
		startString = startString + fmt.Sprintf("### This is row %d\n", i+1)
	}
	return startString
}

func DemoBlogItems() []BlogItem {
	return []BlogItem{
		{0, "My First post", "It's my first post", "# My first post \n\n### Hello there!"},
		{1, "My Second post", "It's my second post", "# My second post \n\n### Hello there!"},
		{2, "My third post", "It's my third post", "# My third post \n\n### Hello there!"},
		{3, "My long post", "It's my long post", GenerateLongItem()},
	}
}

func NewBlogModel(getBlogItems func() []BlogItem, title string, width int, height int, markdownHeight int, liststyle lipgloss.Style) BlogModel {
	r, _ := glamour.NewTermRenderer(
		glamour.WithAutoStyle(),
	)

	m := BlogModel{
		GetBlogItems: getBlogItems,
		List: list.New(
			[]list.Item{},
			list.NewDefaultDelegate(),
			width,
			height,
		),
		Width:            width,
		Height:           height,
		ListRenderer:     liststyle,
		glamourRenderer:  r,
		MarkdownHeight:   markdownHeight,
		contentLineStart: 0,
		contentLineEnd:   markdownHeight - 1,
	}
	m.List.Title = title
	return m
}

func RefreshBlogCommand() tea.Msg {
	return RefreshBlogMsg{}
}

func ConvertBlogItemsToListItems(blogItems []BlogItem) []list.Item {
	listItems := []list.Item{}
	for _, post := range blogItems {
		listItems = append(listItems, item{title: post.Title, desc: post.Description})
	}
	return listItems
}

func (m BlogModel) Init() tea.Cmd {
	return RefreshBlogCommand
}

func (m BlogModel) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	var cmd tea.Cmd
	switch msg := msg.(type) {
	case tea.WindowSizeMsg:
		m.Height = msg.Height
		m.Width = msg.Width
		m.MarkdownHeight = msg.Height - 1
		m.contentLineEnd = m.contentLineStart + m.MarkdownHeight - 1
	case BlogResizeMsg:
		m.Height = msg.height
		m.Width = msg.width
	case RefreshBlogMsg:
		m.BlogItems = m.GetBlogItems()
		m.List.SetItems(ConvertBlogItemsToListItems(m.BlogItems))
		return m, nil
	}
	if m.IsViewing {
		switch msg := msg.(type) {
		case tea.KeyMsg:
			switch msg.String() {
			case "backspace", "esc":
				m.IsViewing = false
				m.contentLineStart = 0
				m.contentLineEnd = m.MarkdownHeight - 1
				return m, nil
			case "ctrl+c":
				return m, tea.Quit
			case "j", "down":
				if m.contentLineEnd != len(m.CurrentContent)-1 {
					m.contentLineStart++
					m.contentLineEnd++
				}
				return m, nil
			case "k", "up":
				if m.contentLineStart != 0 {
					m.contentLineStart--
					m.contentLineEnd--
				}
				return m, nil
			}

		}
	} else {
		switch msg := msg.(type) {
		case tea.KeyMsg:
			switch msg.String() {
			case "enter", "right":
				m.IsViewing = true
				rawContent := m.BlogItems[m.List.Index()].Content
				content, _ := m.glamourRenderer.Render(rawContent)
				m.CurrentContent = strings.Split(content, "\n")
				if m.contentLineEnd >= len(m.CurrentContent) && len(m.CurrentContent) != 0 {
					m.contentLineEnd = len(m.CurrentContent) - 1
				}
				if m.contentLineEnd > len(m.CurrentContent) {
					m.contentLineEnd = len(m.CurrentContent)
				}
				return m, nil
			case "ctrl+c":
				return m, tea.Quit

			}
		}
	}
	m.List, cmd = m.List.Update(msg)
	return m, cmd
}

func (m BlogModel) View() string {
	if m.IsViewing {
		content := m.ListRenderer.Width(m.Width).Align(lipgloss.Center).Render("___________")
		if m.contentLineStart > 0 {
			topCarat := m.ListRenderer.Width(m.Width).Align(lipgloss.Center).Render("^")
			content = lipgloss.JoinVertical(lipgloss.Top, content, topCarat)
		} else {
			topCarat := m.ListRenderer.Width(m.Width).Align(lipgloss.Center).Render("Start")
			content = lipgloss.JoinVertical(lipgloss.Top, content, topCarat)
		}

		out := strings.Join(m.CurrentContent[m.contentLineStart:m.contentLineEnd], "\n")
		content = lipgloss.JoinVertical(lipgloss.Top, content, out)
		if m.contentLineEnd < len(m.CurrentContent)-1 {
			bottomCarat := m.ListRenderer.Width(m.Width).Align(lipgloss.Center).Render("v")
			content = lipgloss.JoinVertical(lipgloss.Top, content, bottomCarat)
		} else {
			bottomCarat := m.ListRenderer.Width(m.Width).Align(lipgloss.Center).Render("End")
			content = lipgloss.JoinVertical(lipgloss.Top, content, bottomCarat)
		}
		bottomContentLine := m.ListRenderer.Width(m.Width).Align(lipgloss.Center).Render("___________")
		content = lipgloss.JoinVertical(lipgloss.Top, content, bottomContentLine)
		return m.ListRenderer.Height(m.Height).Render(content)
	} else {
		m.List.SetHeight(m.Height - 10)
		m.List.SetWidth(m.Width)
		content := m.ListRenderer.PaddingTop(1).Width(m.Width).Align(lipgloss.Center).Render("Enter or right key to view a blog entry (backspace or ESC to exit blog post)")
		listContent := m.ListRenderer.Width(m.Width).Height(m.Height - 3).PaddingTop(1).Render(m.List.View())
		return lipgloss.JoinVertical(lipgloss.Top, content, listContent)
	}
}
